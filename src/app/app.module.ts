import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule, ErrorHandler } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthorizationService } from './authorization.service';
import { HttpBasicInterceptor } from './commons/http/http-basic.interceptor';
import { UrlCompleteInterceptor } from './commons/http/url-complete.interceptor';
import { MessageModule } from './commons/messages/message.module';
import { ScreenModule } from './commons/screen/screen.module';
import { I18nService } from './i18n/i18n.service';
import { NavigationService } from './navigation.service';
import { ErrorHandlerService } from './commons/error/error-handler.service';


export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/');
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    MessageModule,
    MatToolbarModule,
    MatSelectModule,
    MatIconModule,
    ScreenModule
  ],
  providers: [
    AuthorizationService,
    I18nService,
    NavigationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpBasicInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UrlCompleteInterceptor,
      multi: true
    },
    {
      provide: ErrorHandler,
      useClass: ErrorHandlerService
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
