export interface LanguageSupport {
  code: string;
  name: string;
  default: boolean;
}
