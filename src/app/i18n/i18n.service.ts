import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LanguageSupport } from './language-support.model';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class I18nService {
  private readonly LANG_STORAGE_KEY = 'used-lang';
  private readonly APP_LANGUAGES_URL = './assets/resources/languages.json';

  constructor(private httpClient: HttpClient) { }

  public getSupportedLanguages(): Observable<LanguageSupport[]> {
    return this.httpClient.get<LanguageSupport[]>(this.APP_LANGUAGES_URL);
  }

  public getCachedLang(): string {
    return localStorage.getItem(this.LANG_STORAGE_KEY);
  }

  public cacheLang(lang: string): void {
    localStorage.setItem(this.LANG_STORAGE_KEY, lang);
  }
}
