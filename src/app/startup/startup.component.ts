import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'dr-startup',
  templateUrl: './startup.component.html',
  styleUrls: ['./startup.component.scss']
})
export class StartupComponent implements OnInit, AfterViewInit {
  @ViewChild('tabGroup', { read: ElementRef })
  tabGroup: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.focusActiveTab();
  }

  private focusActiveTab(): void {
    const tabGroupDiv: HTMLDivElement = this.tabGroup.nativeElement;
    const activeLabel: HTMLDivElement = tabGroupDiv.querySelector('mat-tab-header > div.mat-tab-label-container div.mat-tab-label-active');
    activeLabel.focus();
  }
}
