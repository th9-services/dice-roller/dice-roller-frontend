export interface RoomConfigDTO {
  allowRoomJoin: boolean;
  autoEnablePlayers: boolean;
  autoEnableDices: boolean;
}
