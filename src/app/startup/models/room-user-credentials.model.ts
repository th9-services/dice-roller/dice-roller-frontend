import { UserCredentialsDTO } from './user-credentials.model';

export interface RoomUserCredentialsDTO {
  roomId: string;
  credentials: UserCredentialsDTO;
}
