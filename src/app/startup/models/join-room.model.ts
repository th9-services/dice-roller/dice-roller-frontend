import { RoomCredentialsDTO } from './room-credentials.model';

export interface JoinRoomDTO {
  roomCredentials: RoomCredentialsDTO;
  playerNickname: string;
}
