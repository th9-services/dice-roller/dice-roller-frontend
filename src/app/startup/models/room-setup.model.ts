import { RoomCredentialsDTO } from './room-credentials.model';
import { RoomConfigDTO } from './room-config.model';

export interface RoomSetupDTO {
  dices: number[];
  roomCredentials: RoomCredentialsDTO;
  playerNickname: string;
  config: RoomConfigDTO;
}
