export interface RoomCredentialsDTO {
  roomName: string;
  roomPassword: string;
}
