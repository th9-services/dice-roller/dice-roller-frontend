import { UserType } from './user-type.enum';

export interface UserCredentialsDTO {
  username: string;
  password: string;
  userType: UserType;
}
