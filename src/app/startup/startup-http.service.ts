import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthorizationService } from '../authorization.service';
import { JoinRoomDTO } from './models/join-room.model';
import { RoomUserCredentialsDTO } from './models/room-user-credentials.model';
import { RoomSetupDTO } from './models/room-setup.model';

@Injectable()
export class StartupHttpService {
  private readonly JOIN_ROOM_URL = '/api/room/join-room';
  private readonly CREATE_ROOM_URL = '/api/room/create-room';

  constructor(private httpClient: HttpClient, private authorizationService: AuthorizationService) { }

  public joinRoom(body: JoinRoomDTO): Observable<RoomUserCredentialsDTO> {
    return this.httpClient.post<RoomUserCredentialsDTO>(this.JOIN_ROOM_URL, body)
      .pipe(
        tap(credentials => this.saveUserCredentials(credentials))
      );
  }

  public createRoom(setup: RoomSetupDTO): Observable<RoomUserCredentialsDTO> {
    return this.httpClient.post<RoomUserCredentialsDTO>(this.CREATE_ROOM_URL, setup)
      .pipe(
        tap(credentials => this.saveUserCredentials(credentials))
      );
  }

  private saveUserCredentials(credentials: RoomUserCredentialsDTO): void {
    this.authorizationService.saveUser(credentials.credentials);
  }

}
