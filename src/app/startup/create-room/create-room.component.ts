import { switchMap } from 'rxjs/operators';
import { BaseDestroyDirective } from 'src/app/base-destroy.directive';

import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { RoomSetupDTO } from '../models/room-setup.model';
import { StartupFormService } from '../startup-form.service';
import { StartupHttpService } from '../startup-http.service';

@Component({
  selector: 'dr-create-room',
  templateUrl: './create-room.component.html',
  styleUrls: [
    './create-room.component.scss',
    '../startup.component.scss'
  ]
})
export class CreateRoomComponent extends BaseDestroyDirective implements OnInit {
  form: FormGroup;
  passwordHide: boolean = true;

  constructor(private startupHttpService: StartupHttpService,
              private startupFormService: StartupFormService,
              private router: Router) {
    super();
  }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    this.startupFormService.createForm()
      .subscribe(form => this.form = form);
  }

  public createRoom(): void {

    if (!this.form.valid) {
      return;
    }

    const setup: RoomSetupDTO = this.prepareSetup();
    this.startupHttpService.createRoom(setup)
      .pipe(
        switchMap(credentials => this.router.navigate(['/room', credentials.roomId]))
      ).subscribe();
  }

  private prepareSetup(): RoomSetupDTO {
    const setup: RoomSetupDTO = this.form.getRawValue();

    return {
      ...setup,
      dices: setup.dices.filter(dice => !!dice)
    };
  }

}
