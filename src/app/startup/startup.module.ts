import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { TranslateModule } from '@ngx-translate/core';
import { DicesFormModule } from '../commons/dices-form/dices-form.module';
import { FormPipesModule } from '../commons/forms/form-pipes.module';
import { CreateRoomComponent } from './create-room/create-room.component';
import { JoinRoomComponent } from './join-room/join-room.component';
import { StartupFormService } from './startup-form.service';
import { StartupHttpService } from './startup-http.service';
import { StartupRoutingModule } from './startup-routing.module';
import { StartupComponent } from './startup.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';


@NgModule({
  declarations: [
    StartupComponent,
    CreateRoomComponent,
    JoinRoomComponent
  ],
  imports: [
    CommonModule,
    StartupRoutingModule,
    TranslateModule.forChild(),
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    FormPipesModule,
    MatTabsModule,
    DicesFormModule,
    MatExpansionModule,
    MatSlideToggleModule
  ],
  providers: [
    StartupHttpService,
    StartupFormService
  ]
})
export class StartupModule { }
