import { Injectable } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DicesFormService } from '../commons/dices-form/dices-form.service';

@Injectable()
export class StartupFormService {

  constructor(private dicesFormService: DicesFormService) {
  }

  public createForm(): Observable<FormGroup> {
    return this.dicesFormService.getDefaultDices()
      .pipe(
        map(dices => dices.map(dice => new FormControl(dice))),
        map(dicesForm => dicesForm.concat(new FormControl())),
        map(diceForms => this.createFormWithDices(diceForms))
      );
  }

  private createFormWithDices(diceForms: FormControl[]): FormGroup {
    return new FormGroup({
      dices: new FormArray(diceForms),
      roomCredentials: new FormGroup({
        roomName: new FormControl('', [Validators.required, Validators.minLength(4)]),
        roomPassword: new FormControl('', [Validators.required, Validators.minLength(4)])
      }),
      playerNickname: new FormControl('', [Validators.required, Validators.minLength(4)]),
      config: this.createDefaultConfig()
    });
  }

  private createDefaultConfig(): FormGroup {
    return new FormGroup({
      allowRoomJoin: new FormControl(true),
      autoEnablePlayers: new FormControl(true),
      autoEnableDices: new FormControl(true)
    });
  }
}
