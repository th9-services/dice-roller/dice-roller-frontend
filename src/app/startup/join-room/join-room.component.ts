import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { StartupHttpService } from '../startup-http.service';

@Component({
  selector: 'dr-join-room',
  templateUrl: './join-room.component.html',
  styleUrls: [
    './join-room.component.scss',
    '../startup.component.scss'
  ]
})
export class JoinRoomComponent implements OnInit {
  form: FormGroup;
  passwordHide = true;

  constructor(private startupService: StartupHttpService,
              private router: Router) { }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    this.form = new FormGroup({
      roomCredentials: new FormGroup({
        roomName: new FormControl('', [Validators.required, Validators.minLength(4)]),
        roomPassword: new FormControl('', [Validators.required, Validators.minLength(4)])
      }),
      playerNickname: new FormControl('', [Validators.required, Validators.minLength(4)])
    });
  }

  public joinRoom(): void {
    if (this.form.valid) {
      this.requestJoinRoom();
    }
  }

  private requestJoinRoom(): void {
    this.startupService.joinRoom(this.form.getRawValue())
      .pipe(
        switchMap(credentials => this.router.navigate(['/room', credentials.roomId]))
      )
      .subscribe();
  }
}
