import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { LoginHttpService } from './login/login-http.service';
import { LoginComponent } from './login/login.component';
import { ManagementRoutingModule } from './management-routing.module';
import { FormPipesModule } from '../commons/forms/form-pipes.module';
import { MatIconModule } from '@angular/material/icon';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    LoginComponent,
    DashboardComponent
  ],
  imports: [
    CommonModule,
    ManagementRoutingModule,
    TranslateModule.forChild(),
    MatTableModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    FormPipesModule,
    MatIconModule
  ],
  providers: [
    LoginHttpService
  ]
})
export class ManagementModule { }
