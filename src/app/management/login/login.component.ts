import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginHttpService } from './login-http.service';
import { LoginCredentials } from './login-credentials.model';
import { switchMap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { of } from 'rxjs';

@Component({
  selector: 'dr-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  passwordHide = true;

  constructor(private loginHttpService: LoginHttpService,
              private router: Router) { }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    this.form = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }


  public login(): void {
    if (this.form.valid) {
      this.requestLogin();
    }
  }

  private requestLogin(): void {
    const credentials: LoginCredentials = this.form.getRawValue();
    this.loginHttpService.login(credentials)
      .pipe(
        switchMap(() => this.router.navigate(['/management', 'dashboard'])),
        catchError(() => of(false))
      )
      .subscribe();
  }
}
