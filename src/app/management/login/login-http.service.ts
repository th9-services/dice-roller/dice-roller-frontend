import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginCredentials } from './login-credentials.model';
import { Observable } from 'rxjs';
import { AuthorizationService } from 'src/app/authorization.service';
import { UserType } from 'src/app/startup/models/user-type.enum';
import { tap } from 'rxjs/operators';
import { HttpConfig } from 'src/app/commons/http/http-config';

@Injectable()
export class LoginHttpService {
  private readonly SUPER_USER_LOGIN_URL = '/api/management/authenticate';

  constructor(private httpClient: HttpClient,
              private authorizationService: AuthorizationService) { }

  public login(credentials: LoginCredentials): Observable<boolean> {
    const headers = HttpConfig.createAuthorizationHeader(credentials.username, credentials.password);
    return this.httpClient.post<boolean>(this.SUPER_USER_LOGIN_URL, credentials, { headers })
      .pipe(tap(() => this.saveSuperUser(credentials)));
  }

  private saveSuperUser(credentials: LoginCredentials): void {
    this.authorizationService.saveUser({ ...credentials, userType: UserType.SUPER_USER });
  }
}
