import { Injectable } from '@angular/core';

export interface NavigationLink {
  url: string;
  name: string;
}

@Injectable()
export class NavigationService {
  private readonly NAVIGATION_STORAGE_KEY = 'navigation';

  constructor() { }

  public saveNavigation(navigation: NavigationLink): void {
    sessionStorage.setItem(this.NAVIGATION_STORAGE_KEY, JSON.stringify(navigation));
  }

  public getNavigation(): NavigationLink {
    try {
      const navigation: string = sessionStorage.getItem(this.NAVIGATION_STORAGE_KEY);
      return JSON.parse(navigation);
    } catch (error) {
      return null;
    }
  }

  public clearNavigation(): void {
    sessionStorage.removeItem(this.NAVIGATION_STORAGE_KEY);
  }
}
