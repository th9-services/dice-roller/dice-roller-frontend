import { ConnectionEvent, EventSourcePolyfill } from 'event-source-polyfill';
import { Observable, Subscriber } from 'rxjs';
import { share, tap } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';

import { AuthorizationService } from '../authorization.service';
import { HttpConfig } from '../commons/http/http-config';
import { HttpParam } from '../commons/http/http-param.enum';
import { NavigationService } from '../navigation.service';
import { UserCredentialsDTO } from '../startup/models/user-credentials.model';
import { GameRoomConfig } from './model/game-room-config.model';
import { GameRoom } from './model/game-room.model';
import { RoomHttpError } from './model/room-http-error.enum';
import { User } from './model/user.model';

@Injectable()
export class RoomHttpService {

  private readonly GET_ROOM_INFO_STREAM_URL = '/api/room/room-info-stream?roomId={roomId}';
  private readonly CLOSE_ROOM_URL = '/api/room/close-room';
  private readonly OPEN_ROOM_URL = '/api/room/open-room';
  private readonly DESTROY_ROOM_URL = '/api/room/destroy-room';
  private readonly KICK_PLAYER_URL = '/api/room/kick-player';
  private readonly UPDATE_DICES_URL = '/api/room/update-dices';
  private readonly UPDATE_CONFIG_URL = '/api/room/update-config';

  private readonly LOCAL_ROOM_INFO_URL = './room/';

  private roomInfoSource: EventSource;

  constructor(private httpClient: HttpClient,
              private zone: NgZone,
              private navigationService: NavigationService,
              private authorizationService: AuthorizationService
  ) { }

  public updateConfig(roomId: string, config: GameRoomConfig): Observable<void> {
    return this.httpClient.patch<void>(this.UPDATE_CONFIG_URL, config, this.createParams(roomId));
  }

  public closeRoom(roomId: string): Observable<void> {
    return this.httpClient.patch<void>(this.CLOSE_ROOM_URL, null, this.createParams(roomId));
  }

  public openRoom(roomId: string): Observable<void> {
    return this.httpClient.patch<void>(this.OPEN_ROOM_URL, null, this.createParams(roomId));
  }

  public destroyRoom(roomId: string): Observable<void> {
    return this.httpClient.patch<void>(this.DESTROY_ROOM_URL, null, this.createParams(roomId))
      .pipe(
        tap(() => this.navigationService.clearNavigation()),
        tap(() => this.authorizationService.clearUser())
      );
  }

  public kickPlayer(roomId: string, player: User): Observable<void> {
    return this.httpClient.patch<void>(this.KICK_PLAYER_URL, player, this.createParams(roomId));
  }

  private createParams(roomId: string): { [param: string]: any } {
    return {
      params: {
        [HttpParam.ROOM_ID]: roomId
      }
    };
  }

  public closeRoomInfoStream(): void {
    if (this.roomInfoSource) {
      this.roomInfoSource.close();
    }
  }

  public updateDices(roomId: string, dices: number[]): Observable<void> {
    return this.httpClient.patch<void>(this.UPDATE_DICES_URL, dices, this.createParams(roomId));
  }

  public getRoomInfoStream(roomId: string): Observable<GameRoom> {
    return new Observable<GameRoom>(subscriber => this.setUpSubscriber(subscriber, roomId))
      .pipe(
        tap(room => this.validate(room)),
        tap(room => this.navigationService.saveNavigation({ name: room.roomName, url: this.LOCAL_ROOM_INFO_URL + room.roomId })),
        share()
      );
  }

  private validate(room: GameRoom): void {
    if (!this.authorizationService.isCurrentUserInRoom(room)) {
      this.authorizationService.clearUser();
      this.navigationService.clearNavigation();
      throw Error(RoomHttpError.KICKED_FROM_ROOM);
    }
  }

  private setUpSubscriber(subscriber: Subscriber<GameRoom>, roomId: string): void {
    this.roomInfoSource = this.createRoomInfoSource(roomId);

    this.roomInfoSource.onmessage = sse => this.zone.run(
      () => subscriber.next(this.parseMessageEvent(sse))
    );

    this.roomInfoSource.onerror = err =>
      this.handleInfoStreamError(err, subscriber);
  }

  private createRoomInfoSource(roomId: string): EventSourcePolyfill {
    const url = HttpConfig.hostUrl + this.GET_ROOM_INFO_STREAM_URL.replace('{roomId}', roomId);

    return new EventSourcePolyfill(url, {
      headers: this.createEventSourceHeaders()
    });
  }

  private createEventSourceHeaders(): { [key: string]: string } {
    if (!this.authorizationService.isUserPresent()) {
      throw Error(RoomHttpError.NO_CREDENTIALS);
    }

    const user: UserCredentialsDTO = this.authorizationService.getUser();
    return HttpConfig.createAuthorizationObject(user.username, user.password);
  }

  private parseMessageEvent(event: MessageEvent): GameRoom {
    return JSON.parse(event.data);
  }

  private handleInfoStreamError(err: Event, subscriber: Subscriber<GameRoom>): void {
    if (this.isConnectionEvent(err)) {
      err.preventDefault();
      this.navigationService.clearNavigation();
      this.authorizationService.clearUser();
      subscriber.error(new Error(RoomHttpError.ACCESS_UNATHORIZED));
    }
  }

  private isConnectionEvent(value: Event): value is ConnectionEvent {
    return value.hasOwnProperty('type') &&
      value.hasOwnProperty('status') &&
      value.hasOwnProperty('statusText');
  }
}
