import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TranslateModule } from '@ngx-translate/core';
import { DicesFormModule } from '../commons/dices-form/dices-form.module';
import { FormPipesModule } from '../commons/forms/form-pipes.module';
import { ReversePipe } from './commons/reverse.pipe';
import { RoomHttpService } from './room-http.service';
import { ActualRoundComponent } from './room-info/actual-round/actual-round.component';
import { GameLogComponent } from './room-info/game-log/game-log.component';
import { AddCountComponent } from './room-info/make-roll/add-count/add-count.component';
import { MakeRollComponent } from './room-info/make-roll/make-roll.component';
import { MakeRollService } from './room-info/make-roll/make-roll.service';
import { PlayersComponent } from './room-info/players/players.component';
import { RoomInfoComponent } from './room-info/room-info.component';
import { RoundHttpService } from './room-info/round-http.service';
import { SummaryComponent } from './room-info/summary/summary.component';
import { RoomRoutingModule } from './room-routing.module';
import {MatExpansionModule} from '@angular/material/expansion';
import { DiceTotalsPipe } from './room-info/game-log/dice-totals.pipe';
import { SummaryFormService } from './room-info/summary/summary-form.service';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';


@NgModule({
  declarations: [
    RoomInfoComponent,
    SummaryComponent,
    ActualRoundComponent,
    PlayersComponent,
    ReversePipe,
    MakeRollComponent,
    AddCountComponent,
    GameLogComponent,
    DiceTotalsPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RoomRoutingModule,
    TranslateModule.forChild(),
    MatCardModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatInputModule,
    FormPipesModule,
    MatFormFieldModule,
    DicesFormModule,
    MatTooltipModule,
    MatExpansionModule,
    MatSlideToggleModule
  ],
  providers: [
    RoomHttpService,
    RoundHttpService,
    MakeRollService,
    SummaryFormService
  ]
})
export class RoomModule { }
