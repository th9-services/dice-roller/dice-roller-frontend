import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'dr-add-count',
  templateUrl: './add-count.component.html',
  styleUrls: ['./add-count.component.scss']
})
export class AddCountComponent implements OnInit {
  private diceCounts: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  private maxCount: number = 50;

  @Input()
  countForm: FormControl;

  filteredCounts: Observable<number[]>;
  autocompleteForm: FormControl;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    if (this.route.snapshot.params.diceCounts) {
      this.diceCounts = this.route.snapshot.params.diceCounts;
    }
    this.initAutocompleteForm();
    this.initCountsFilter();
  }

  private initAutocompleteForm(): void {
    this.autocompleteForm = new FormControl(this.countForm.value, this.countForm.validator);
  }

  private initCountsFilter(): void {
    this.filteredCounts = this.autocompleteForm.valueChanges
      .pipe(
        startWith(null),
        map(count => this.filterCounts(count))
      );
  }

  private filterCounts(value: number): number[] {
    let filtered = this.createSuggestion(value)
      .concat(this.diceCounts)
      .filter(dice => dice <= this.maxCount);

    filtered = [... new Set(filtered)];

    if (value) {
      filtered = filtered
        .filter(dice => dice.toString().includes(value.toString()));
    }

    return filtered.sort((a, b) => a - b);
  }

  private createSuggestion(value: number): number[] {
    if (!this.isCountValid(value)) {
      return [];
    }

    const stringValue = value.toString();
    const lastDigit = stringValue.charAt(stringValue.length - 1);
    return [
      value,
      parseInt(stringValue + lastDigit, 10),
      value * 10,
      value * 100,
    ];
  }

  public addCount(count: number): void {
    this.diceCounts.push(count);
    this.countForm.setValue(count);
    this.autocompleteForm.setValue(count);
  }

  public saveSelectedOption(event: MatAutocompleteSelectedEvent): void {
    this.countForm.setValue(event.option.value);
  }

  public onInputBlur(): void {
    if (this.isCountValid(this.autocompleteForm.value)) {
      this.countForm.setValue(this.autocompleteForm.value);
    }
  }

  private isCountValid(value: number): boolean {
    return value > 0 && value <= this.maxCount;
  }
}
