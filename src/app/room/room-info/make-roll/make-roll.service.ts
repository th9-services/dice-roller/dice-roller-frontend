import { Injectable } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { GameRoundRules } from '../../model/game-round-rules.model';
import { RollGroup } from './roll-group.model';

@Injectable()
export class MakeRollService {
  private readonly KEEP_SELECTED_KEY = 'keep-selected';

  constructor() { }

  public createKeepSelectedForm(): FormControl {
    return new FormControl(this.getKeepSelectedValue());
  }

  public getKeepSelectedValue(): boolean {
    try {
      const user: string = localStorage.getItem(this.KEEP_SELECTED_KEY);
      return JSON.parse(user);
    } catch (error) {
      return false;
    }
  }

  public saveKeepSelectedValue(keepSelected: boolean): void {
    localStorage.setItem(this.KEEP_SELECTED_KEY, JSON.stringify(keepSelected));
  }

  public createForm(rules: GameRoundRules): FormArray {
    if (!rules) {
      return null;
    }

    const form = new FormArray([]);
    rules.dices
      .map(dice => new FormGroup({
        active: new FormControl(false),
        dice: new FormControl(dice),
        count: new FormControl(1, Validators.min(1))
      }))
      .forEach(group => form.push(group));

    return form;
  }

  public flattenGroups(groups: RollGroup[]): number[] {
    return groups.reduce(
      (dices, group) => this.join(dices, group),
      [] as number[]
    );
  }

  private join(dices: number[], group: RollGroup): number[] {
    return this.createDiceArray(group.dice, group.count)
      .concat(dices);
  }

  private createDiceArray(dice: number, count: number): number[] {
    const dices: number[] = [];
    for (let i = 0; i < count; ++i) {
      dices.push(dice);
    }
    return dices;
  }
}
