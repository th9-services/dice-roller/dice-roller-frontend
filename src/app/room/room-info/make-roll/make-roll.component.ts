import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AuthorizationService } from 'src/app/authorization.service';
import { ObjectUtils } from 'src/app/commons/object.utils';
import { GameRoundRules } from '../../model/game-round-rules.model';
import { RoundHttpService } from '../round-http.service';
import { MakeRollService } from './make-roll.service';
import { RollGroup } from './roll-group.model';


@Component({
  selector: 'dr-make-roll',
  templateUrl: './make-roll.component.html',
  styleUrls: ['./make-roll.component.scss']
})
export class MakeRollComponent implements OnInit, OnChanges {
  private lastRoundRules: GameRoundRules;

  isForActualPlayer: boolean;
  isGameMaster: boolean;
  dicesForm: FormArray;
  isRollDisabled: boolean = true;
  keepSelected: FormControl;

  @Input()
  rules: GameRoundRules;


  constructor(private authorizationService: AuthorizationService,
              private route: ActivatedRoute,
              private makeRollService: MakeRollService,
              private roundHttpService: RoundHttpService) { }

  ngOnInit(): void {
    if (this.authorizationService.isUserPresent()) {
      this.isGameMaster = this.authorizationService.isGameMaster();
    }

    this.setupKeepSelectedForm();
  }

  private setupKeepSelectedForm(): void {
    this.keepSelected = this.makeRollService.createKeepSelectedForm();
    this.keepSelected.valueChanges
      .subscribe(value => this.makeRollService.saveKeepSelectedValue(value));
  }

  ngOnChanges(): void {
    this.updateRoundState();

    if (this.isFormUpdateRequired()) {
      this.initDicesForm();
      this.updateIsRollDisabled();
      this.lastRoundRules = ObjectUtils.clone(this.rules);
    }
  }

  private updateRoundState(): void {
    if (this.authorizationService.isUserPresent() && this.rules) {
      this.isForActualPlayer = this.rules.players
        .some(player => player.username === this.authorizationService.getUser().username);
    }
  }

  private isFormUpdateRequired(): boolean {
    return !this.dicesForm
      || !ObjectUtils.equals(this.lastRoundRules, this.rules);
  }

  private initDicesForm(): void {
    this.dicesForm = this.makeRollService.createForm(this.rules);
  }

  public toggleDice(dice: FormGroup): void {
    const active: AbstractControl = dice.get('active');

    active.setValue(!active.value);
    this.updateIsRollDisabled();
  }

  public makeRoll(): void {
    const dices: number[] = this.createRoll();

    this.roundHttpService.makeRoll(this.route.snapshot.params.id, dices)
      .subscribe(() => {
        if (!this.keepSelected.value) {
          this.initDicesForm();
        }
        this.updateIsRollDisabled();
      });
  }

  private updateIsRollDisabled(): void {
    this.isRollDisabled = (!this.isForActualPlayer && !this.isGameMaster)
      || !this.isAnyDiceSelected();
  }

  private isAnyDiceSelected(): boolean {
    return (this.dicesForm.getRawValue() as RollGroup[])
      .some(group => group.active && !!group.count);
  }

  private createRoll(): number[] {
    const activeGroups: RollGroup[] = (this.dicesForm.getRawValue() as RollGroup[])
      .filter(group => group.active);

    return this.makeRollService.flattenGroups(activeGroups);
  }

}
