export interface RollGroup {
  active: boolean;
  dice: number;
  count: number;
}
