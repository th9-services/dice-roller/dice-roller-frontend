import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParam } from 'src/app/commons/http/http-param.enum';
import { DiceRollResults } from '../model/dice-roll-results.model';
import { GameRoundRules } from '../model/game-round-rules.model';

@Injectable()
export class RoundHttpService {
  private readonly UPDATE_RULES_URL = '/api/round/update-rules';
  private readonly MAKE_ROLL_URL = '/api/round/make-roll';

  constructor(private httpClient: HttpClient) { }

  public updateRules(roomId: string, rules: GameRoundRules): Observable<void> {
    return this.httpClient.post<void>(this.UPDATE_RULES_URL, rules, this.createParams(roomId));
  }

  public makeRoll(roomId: string, selectedDices: number[]): Observable<DiceRollResults> {
    return this.httpClient.post<DiceRollResults>(this.MAKE_ROLL_URL, { dices: selectedDices }, this.createParams(roomId));
  }

  private createParams(roomId: string): {[param: string]: any} {
    return {
      params: {
        [HttpParam.ROOM_ID]: roomId
      }
    };
  }
}
