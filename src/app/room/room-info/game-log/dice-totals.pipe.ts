import { Pipe, PipeTransform } from '@angular/core';
import { DiceRoll } from '../../model/dice-roll.model';

interface DiceTotals {
  total: number;
  dices: Map<number, number>;
}

@Pipe({
  name: 'diceTotals'
})
export class DiceTotalsPipe implements PipeTransform {

  transform(value: DiceRoll[]): DiceTotals {
    const dices = value
      .reduce(this.addRollToResult, new Map<number, number>());
    const total = Array.from(dices.values())
      .reduce((sum, val) => sum + val, 0);

    return { dices, total };
  }

  private addRollToResult(result: Map<number, number>, roll: DiceRoll): Map<number, number> {
    const current: number = result.get(roll.max) || 0;
    result.set(roll.max, current + roll.result);
    return result;
  }
}
