import { Component, OnInit, Input } from '@angular/core';
import { GameLog } from '../../model/game-log.model';
import { User } from '../../model/user.model';
import { ScreenSizeService } from 'src/app/commons/screen/screen-size.service';
import { ScreenWidthSize } from 'src/app/commons/screen/screen-width-size.enum';

@Component({
  selector: 'dr-game-log',
  templateUrl: './game-log.component.html',
  styleUrls: ['./game-log.component.scss']
})
export class GameLogComponent implements OnInit {
  @Input()
  gameLog: GameLog;
  @Input()
  gameMaster: User;

  screenWidth: ScreenWidthSize;
  ScreenWidthSize = ScreenWidthSize;

  constructor(private screenSizeService: ScreenSizeService) { }

  ngOnInit() {
    this.screenWidth = this.screenSizeService.getCurrentScreenSize().widthSize;
    this.screenSizeService.getScreenResizeStream()
      .subscribe(value => this.screenWidth = value.widthSize);
  }

}
