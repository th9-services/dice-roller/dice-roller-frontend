import { Injectable } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { GameRoomConfig } from '../../model/game-room-config.model';

@Injectable()
export class SummaryFormService {

  public createDicesForm(dices: number[]): FormArray {
    const controls: FormControl[] = dices
      .map(dice => new FormControl(dice))
      .concat(new FormControl());

    return new FormArray(controls);
  }

  public createConfigForm(config: GameRoomConfig): FormGroup {
    return new FormGroup({
      allowRoomJoin: new FormControl(config.allowRoomJoin),
      autoEnablePlayers: new FormControl(config.autoEnablePlayers),
      autoEnableDices: new FormControl(config.autoEnableDices)
    });
  }
}
