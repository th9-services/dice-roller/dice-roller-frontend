import { Observable } from 'rxjs';
import { debounceTime, filter, switchMap } from 'rxjs/operators';
import { AuthorizationService } from 'src/app/authorization.service';
import { MessageService } from 'src/app/commons/messages/message.service';

import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { GameRoomConfig } from '../../model/game-room-config.model';
import { GameRoom } from '../../model/game-room.model';
import { RoomHttpService } from '../../room-http.service';
import { SummaryFormService } from './summary-form.service';

@Component({
  selector: 'dr-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {

  isGameMaster: boolean;
  isDiceEditOpen: boolean;

  dicesForm: FormArray;
  configForm: FormGroup;

  @Input()
  roomInfo: GameRoom;

  constructor(private authorizationService: AuthorizationService,
              private translateService: TranslateService,
              private messageService: MessageService,
              private router: Router,
              private route: ActivatedRoute,
              private summaryFormService: SummaryFormService,
              private roomHttpService: RoomHttpService) { }

  ngOnInit(): void {
    if (this.authorizationService.isUserPresent()) {
      this.isGameMaster = this.authorizationService.isGameMaster();
    }

    this.configForm = this.summaryFormService.createConfigForm(this.roomInfo.config);

    this.configForm.valueChanges
      .pipe(
        debounceTime(300),
        switchMap(() => this.updateConfig())
      ).subscribe();
  }

  private get roomId(): string {
    return this.route.snapshot.params.id;
  }

  private updateConfig(): Observable<void> {
    const config: GameRoomConfig = this.configForm.getRawValue();
    return this.roomHttpService.updateConfig(this.roomId, config);
  }

  public openRoom(): void {
    this.roomHttpService.openRoom(this.roomId)
      .subscribe();
  }

  public closeRoom(): void {
    this.roomHttpService.closeRoom(this.roomId)
      .subscribe();
  }

  public destroyRoom(): void {
    this.translateService.get('gameRoom.summary.destroyRoom.confirm')
      .pipe(
        switchMap(message => this.messageService.confirm(message)),
        filter(confirmed => !!confirmed),
        switchMap(() => this.roomHttpService.destroyRoom(this.roomId)),
        switchMap(() => this.router.navigate(['']))
      ).subscribe();
  }

  public openDiceEdit(): void {
    this.dicesForm = this.summaryFormService.createDicesForm(this.roomInfo.dices);
    this.isDiceEditOpen = true;
  }

  public closeDiceEdit(): void {
    this.isDiceEditOpen = false;
  }

  public updateDices(): void {
    const dices: number[] = this.dicesForm.getRawValue()
      .filter(dice => !!dice)
      .sort((a, b) => a - b);

    this.roomHttpService.updateDices(this.roomId, [...new Set(dices)])
      .subscribe(() => this.closeDiceEdit());
  }
}
