import { takeUntil } from 'rxjs/operators';
import { BaseDestroyDirective } from 'src/app/base-destroy.directive';
import { MessageService } from 'src/app/commons/messages/message.service';

import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { GameRoom } from '../model/game-room.model';
import { RoomHttpError } from '../model/room-http-error.enum';
import { RoomHttpService } from '../room-http.service';

@Component({
  selector: 'dr-room-info',
  templateUrl: './room-info.component.html',
  styleUrls: ['./room-info.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RoomInfoComponent extends BaseDestroyDirective implements OnInit, OnDestroy {
  roomInfo: GameRoom;

  constructor(private roomHttpService: RoomHttpService,
              private route: ActivatedRoute,
              private messageService: MessageService,
              private translateService: TranslateService,
              private router: Router
  ) {
    super();
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.getRoomInfo(id);
  }


  private getRoomInfo(id: string) {
    this.roomHttpService.getRoomInfoStream(id)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(roomInfo => this.roomInfo = roomInfo,
        error => this.onRoomInfoError(error));
  }

  private onRoomInfoError(error: Error): void {
    this.roomHttpService.closeRoomInfoStream();
    this.router.navigate(['']);

    if (error.message === RoomHttpError.KICKED_FROM_ROOM) {
      const message = this.translateService.instant('gameRoom.players.kicked.info');
      this.messageService.info(message);
    }

    if (error.message === RoomHttpError.ACCESS_UNATHORIZED) {
      const message = this.translateService.instant('gameRoom.destroyed.info');
      this.messageService.info(message);
    }
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.roomHttpService.closeRoomInfoStream();
  }
}
