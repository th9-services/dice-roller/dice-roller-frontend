import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../model/user.model';
import { AuthorizationService } from 'src/app/authorization.service';
import { RoomHttpService } from '../../room-http.service';
import { MessageService } from 'src/app/commons/messages/message.service';
import { switchMap, filter } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'dr-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss']
})
export class PlayersComponent implements OnInit {
  isGameMaster: boolean;

  @Input()
  players: User[];

  constructor(private authorizationService: AuthorizationService,
              private messageService: MessageService,
              private route: ActivatedRoute,
              private translateService: TranslateService,
              private roomHttpService: RoomHttpService) { }

  ngOnInit(): void {
    if (this.authorizationService.isUserPresent()) {
      this.isGameMaster = this.authorizationService.isGameMaster();
    }
  }

  public kickPlayer(player: User): void {
    this.translateService.get('gameRoom.players.kick.confirm', { player: player.nickname })
      .pipe(
        switchMap(message => this.messageService.confirm(message)),
        filter(confirmed => !!confirmed),
        switchMap(() => this.roomHttpService.kickPlayer(this.route.snapshot.params.id, player))
      ).subscribe();
  }
}
