import { Observable, Subject } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';
import { AuthorizationService } from 'src/app/authorization.service';

import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { GameRoom } from '../../model/game-room.model';
import { GameRoundRules } from '../../model/game-round-rules.model';
import { User } from '../../model/user.model';
import { RoundHttpService } from '../round-http.service';

interface Selector {
  [key: string]: boolean;
}

@Component({
  selector: 'dr-actual-round',
  templateUrl: './actual-round.component.html',
  styleUrls: ['./actual-round.component.scss']
})
export class ActualRoundComponent implements OnInit, OnChanges {
  private rulesChangeStream: Subject<void> = new Subject();

  selectedPlayers: Selector = {};
  selectedDices: Selector = {};
  isGameMaster: boolean;
  isAnyDiceSelected: boolean = false;

  @Input()
  roomInfo: GameRoom;

  constructor(private roundHttpService: RoundHttpService,
              private authorizationService: AuthorizationService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    if (this.authorizationService.isUserPresent()) {
      this.isGameMaster = this.authorizationService.isGameMaster();
    }

    this.rulesChangeStream
      .pipe(
        debounceTime(300),
        switchMap(() => this.updateRules())
      ).subscribe();
  }

  ngOnChanges(): void {
    this.roomInfo.rules.players
      .forEach(player => this.selectedPlayers[player.username] = true);

    this.roomInfo.rules.dices
      .forEach(dice => this.selectedDices[dice] = true);
  }

  public togglePlayer(player: User): void {
    this.selectedPlayers[player.username] = !this.selectedPlayers[player.username];
    this.rulesChangeStream.next();
  }

  public toggleDice(dice: number): void {
    this.selectedDices[dice] = !this.selectedDices[dice];
    this.isAnyDiceSelected = Object.keys(this.selectedDices).some(key => this.selectedDices[key]);
    this.rulesChangeStream.next();
  }

  private updateRules(): Observable<void> {
    return this.roundHttpService.updateRules(this.route.snapshot.params.id, this.createRules());
  }

  private createRules(): GameRoundRules {
    const dices: number[] = Object.keys(this.selectedDices)
      .filter(dice => !!this.selectedDices[dice])
      .map(dice => parseInt(dice, 10));

    const players: User[] = Object.keys(this.selectedPlayers)
      .filter(username => !!this.selectedPlayers[username])
      .map(username => this.roomInfo.players.find(player => player.username === username))
      .filter(player => !!player);

    return { dices, players };
  }
}
