import { User } from './user.model';

export interface GameRoundRules {
  dices: number[];
  players: User[];
}
