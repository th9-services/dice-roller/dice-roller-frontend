export interface GameRoomConfig {
  allowRoomJoin: boolean;
  autoEnablePlayers: boolean;
  autoEnableDices: boolean;
}
