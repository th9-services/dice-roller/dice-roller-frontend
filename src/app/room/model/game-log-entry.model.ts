import { User } from './user.model';
import { DiceRollResults } from './dice-roll-results.model';

export interface GameLogEntry {
  rolls: DiceRollResults;
  time: Date;
  player: User;
}
