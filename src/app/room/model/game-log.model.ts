import { GameLogEntry } from './game-log-entry.model';

export interface GameLog {
  entries: GameLogEntry[];
}
