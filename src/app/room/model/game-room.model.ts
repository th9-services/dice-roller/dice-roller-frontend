import { GameLog } from './game-log.model';
import { GameRoomConfig } from './game-room-config.model';
import { GameRoundRules } from './game-round-rules.model';
import { User } from './user.model';

export interface GameRoom {
  roomId: string;
  dices: number[];
  players: User[];
  rules: GameRoundRules;
  gameMaster: User;
  roomName: string;
  gameLog: GameLog;
  config: GameRoomConfig;
}
