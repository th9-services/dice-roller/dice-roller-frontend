export interface DiceRoll {
  max: number;
  result: number;
}
