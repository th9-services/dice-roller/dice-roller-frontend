import { DiceRoll } from './dice-roll.model';

export interface DiceRollResults {
  results: DiceRoll[];
}
