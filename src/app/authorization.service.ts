import { Injectable } from '@angular/core';
import { UserCredentialsDTO } from './startup/models/user-credentials.model';
import { UserType } from './startup/models/user-type.enum';
import { GameRoom } from './room/model/game-room.model';


@Injectable()
export class AuthorizationService {
  private readonly USER_STORAGE_KEY = 'logged-user';


  constructor() { }

  public saveUser(user: UserCredentialsDTO): void {
    sessionStorage.setItem(this.USER_STORAGE_KEY, JSON.stringify(user));
  }

  public getUser(): UserCredentialsDTO {
    try {
      const user: string = sessionStorage.getItem(this.USER_STORAGE_KEY);
      return JSON.parse(user);
    } catch (error) {
      return null;
    }
  }

  public clearUser(): void {
    sessionStorage.removeItem(this.USER_STORAGE_KEY);
  }

  public isUserPresent(): boolean {
    return !!this.getUser();
  }

  public isGameMaster(): boolean {
    return this.getUser().userType === UserType.GAME_MASTER;
  }

  public isCurrentUserInRoom(room: GameRoom): boolean {
    const user: UserCredentialsDTO = this.getUser();

    return !!room.players.find(player => player.username === user.username)
    || room.gameMaster.username === user.username;
  }
}
