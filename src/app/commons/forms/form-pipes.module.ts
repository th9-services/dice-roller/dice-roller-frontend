import { NgModule } from '@angular/core';
import { ErrorFormatPipe } from './error-format.pipe';
import { IsPositivePipe } from './is-positive.pipe';

@NgModule({
  declarations: [
    ErrorFormatPipe,
    IsPositivePipe
  ],
  exports: [
    ErrorFormatPipe,
    IsPositivePipe
  ],
  imports: [
  ],
  providers: [
    ErrorFormatPipe,
    IsPositivePipe
  ]
})
export class FormPipesModule { }
