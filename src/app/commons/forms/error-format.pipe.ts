import { Pipe, PipeTransform } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

@Pipe({
  name: 'errorFormat'
})
export class ErrorFormatPipe implements PipeTransform {

  constructor(private translateService: TranslateService) {
  }

  transform(errors: ValidationErrors): Observable<string> {
    if (errors.hasOwnProperty('required')) {
      return this.translateService.get('form.error.required');
    }
    if (errors.hasOwnProperty('minlength')) {
      return this.translateService.get('form.error.minlength', { value: errors.minlength.requiredLength });

    }
    return this.translateService.get('form.error.unknown');
  }

}
