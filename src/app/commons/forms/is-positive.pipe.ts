import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isPositive'
})
export class IsPositivePipe implements PipeTransform {

  transform(value: number): boolean {
    return !!value
      && !isNaN(value)
      && Number.isInteger(value)
      && value > 1;
  }

}
