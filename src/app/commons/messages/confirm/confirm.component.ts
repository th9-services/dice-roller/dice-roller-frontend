import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'dr-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  constructor(private dialgoRef: MatDialogRef<ConfirmComponent>,
              @Inject(MAT_DIALOG_DATA) public message: string) { }

  ngOnInit(): void {
  }

  public confirm(): void {
    this.close(true);
  }

  public cancel(): void {
    this.close(false);
  }

  private close(result: boolean): void {
    this.dialgoRef.close(result);
  }
}
