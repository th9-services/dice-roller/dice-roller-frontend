import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import { ConfirmComponent } from './confirm/confirm.component';
import { MessageService } from './message.service';
import { InformationComponent } from './information/information.component';
import { ErrorComponent } from './error/error.component';



@NgModule({
  declarations: [
    ConfirmComponent,
    InformationComponent,
    ErrorComponent
  ],
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    MatDialogModule,
    MatButtonModule
  ],
  providers: [
    MessageService
  ]
})
export class MessageModule { }
