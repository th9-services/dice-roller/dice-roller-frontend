import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { ConfirmComponent } from './confirm/confirm.component';
import { ErrorComponent } from './error/error.component';
import { InformationComponent } from './information/information.component';

type DialogComponent = ConfirmComponent | ErrorComponent | InformationComponent;

@Injectable()
export class MessageService {
  private dialogReference: MatDialogRef<DialogComponent, any>;

  constructor(private dialog: MatDialog) { }

  public info(message: string): Observable<void> {
    if (this.isNewMessage(message)) {
      this.dialog.closeAll();
      this.openInfoDialog(message);
      return this.getDialogResult();
    }
  }

  private openInfoDialog(message: string): void {
    this.dialogReference = this.dialog.open<InformationComponent, string, void>(
      InformationComponent,
      { data: message, minWidth: 400, minHeight: 200, maxWidth: 900, panelClass: 'message-info' }
    );
  }

  public error(message: string): Observable<void> {
    if (this.isNewMessage(message)) {
      this.dialog.closeAll();
      this.openErrorDialog(message);
      return this.getDialogResult();
    }
  }

  private openErrorDialog(message: string): void {
    this.dialogReference = this.dialog.open<ErrorComponent, string, void>(
      ErrorComponent,
      { data: message, minWidth: 400, minHeight: 200, maxWidth: 900, panelClass: 'message-error' }
    );
  }

  public confirm(message: string): Observable<boolean> {
    if (this.isNewMessage(message)) {
      this.dialog.closeAll();
      this.openConfirmDialog(message);
      return this.getDialogResult<boolean>();
    }
  }

  private openConfirmDialog(message: string): void {
    this.dialogReference = this.dialog.open<ConfirmComponent, string, boolean>(
      ConfirmComponent,
      { data: message, minWidth: 400, minHeight: 200, maxWidth: 900, panelClass: 'message-confirm' }
    );
  }

  private isNewMessage(message: string): boolean {
    return this.dialogReference?.componentInstance?.message !== message;
  }

  private getDialogResult<T>(): Observable<T> {
    return this.dialogReference.afterClosed()
      .pipe(
        tap(() => this.dialogReference = null)
      );
  }
}
