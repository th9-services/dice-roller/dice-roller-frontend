import { environment } from 'src/environments/environment';
import { HttpHeaders } from '@angular/common/http';

export class HttpConfig {
  static readonly hostUrl = environment.hostUrl;
  static readonly authorizationHeaderName = 'Authorization';
  static readonly xRequestedHeaderName = 'X-Requested-With';
  static readonly xRequestedHeaderValue = 'XMLHttpRequest';


  static createAuthorizationHeader(username: string, password: string): HttpHeaders {
    return new HttpHeaders(
      this.createAuthorizationObject(username, password)
    );
  }

  static createAuthorizationObject(username: string, password: string): { [key: string]: string } {
    return {
      [HttpConfig.xRequestedHeaderName]: HttpConfig.xRequestedHeaderValue,
      [HttpConfig.authorizationHeaderName]: 'Basic ' + btoa(username + ':' + password),
    };
  }
}
