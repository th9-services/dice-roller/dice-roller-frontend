import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthorizationService } from 'src/app/authorization.service';
import { UserCredentialsDTO } from 'src/app/startup/models/user-credentials.model';
import { HttpConfig } from './http-config';

@Injectable()
export class HttpBasicInterceptor implements HttpInterceptor {

  constructor(private authorizationService: AuthorizationService) {

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.authorizationService.isUserPresent() && !this.isLocal(request)) {
      const user: UserCredentialsDTO = this.authorizationService.getUser();
      request = request.clone({
        headers: this.createAuthorizationHeaders(user)
      });
    }

    return next.handle(request);
  }

  private isLocal(request: HttpRequest<any>): boolean {
    return request.url.includes('/assets');
  }

  private createAuthorizationHeaders(user: UserCredentialsDTO): HttpHeaders {
    return HttpConfig.createAuthorizationHeader(user.username, user.password);
  }

}
