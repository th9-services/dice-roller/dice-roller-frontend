import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpConfig } from './http-config';

@Injectable()
export class UrlCompleteInterceptor implements HttpInterceptor {

  constructor(private route: ActivatedRoute) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.isLocal(request)) {
      return next.handle(request);
    }


    return next.handle(
      request.clone({
        url: this.addHostUrl(request.url)
      })
    );
  }

  private isLocal(request: HttpRequest<any>): boolean {
    return request.url.includes('/assets');
  }

  private addHostUrl(url: string): string {
    return HttpConfig.hostUrl + url;
  }

}
