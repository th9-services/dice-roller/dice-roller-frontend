import { HttpErrorField } from './http-error-field.model';

export interface HttpErrorDetails {
  error: string;
  errors?: HttpErrorField[];
  message: string;
  path: string;
  status: number;
  timestamp: number;
}
