import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable, Injector, NgZone } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { MessageService } from '../messages/message.service';
import { HttpErrorDetails } from './http-error-details.model';
import { HttpErrorMessages } from './http-error-messages.enum';
import { InternalErrorMessages } from './internal-error-messages.enum';

@Injectable()
export class ErrorHandlerService implements ErrorHandler {
  private translateService: TranslateService;

  constructor(private messageService: MessageService,
              private injector: Injector,
              private zone: NgZone) {
  }

  public handleError(error: Error | HttpErrorResponse): void {
    this.zone.run(() => {
      this.translateService = this.injector.get(TranslateService);

      if (error instanceof HttpErrorResponse) {
        this.handleHttpError(error);
      } else {
        this.handleInternalError(error);
      }
    });
  }

  private handleHttpError(error: HttpErrorResponse): void {
    const errorDetails: HttpErrorDetails = error.error;

    if (this.isKnownHttpError(errorDetails)) {
      this.handleKnownHttpError(errorDetails);
    } else {
      const message: string = this.translateService.instant('app.error.http.unknown');
      this.messageService.error(message);
      console.error(error);
    }
  }

  private isKnownHttpError(error: HttpErrorDetails): boolean {
    return HttpErrorMessages.contains(error.message);
  }

  private handleKnownHttpError(error: HttpErrorDetails): void {
    if (HttpErrorMessages.Values.INVALID_DATA === error.message) {
      const message = error.errors
        .map(field => this.translateService.instant('app.error.http.' + error.message + '.' + field.reason))
        .reduce((total, fieldMessage) => total + fieldMessage + ' ', '');
      this.messageService.error(message);
    } else {
      const message: string = this.translateService.instant('app.error.http.' + error.message);
      this.messageService.error(message);
    }
  }

  private handleInternalError(error: Error): void {
    if (this.isKnownInternalError(error)) {
      this.handleKnownInternalError(error);
    } else {
      const message: string = this.translateService.instant('app.error.internal.unknown');
      this.messageService.error(message);
      console.error(error);
    }
  }

  private isKnownInternalError(error: Error): boolean {
    return InternalErrorMessages.contains(error.message);
  }

  private handleKnownInternalError(error: Error): void {
    if (this.isSSETimeoutError(error)) {
      return;
    }
    if (InternalErrorMessages.Values.FAILED_TO_FETCH_SSE === error.message) {
      const message: string = this.translateService.instant('app.error.internal.sse.failed');
      this.messageService.error(message);
    } else {
      const message: string = this.translateService.instant('app.error.internal.' + error.message);
      this.messageService.error(message);
    }
  }

  private isSSETimeoutError(error: Error): boolean {
    return error.message.includes(InternalErrorMessages.Values.SSE_TIMEOUT);
  }
}
