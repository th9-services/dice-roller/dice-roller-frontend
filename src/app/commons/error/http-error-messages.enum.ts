
enum Values {
  ROOM_NOT_FOUND = 'ROOM_NOT_FOUND',
  INVALID_DATA = 'INVALID_DATA',
  INVALID_PLAYER = 'INVALID_PLAYER',
  ROOM_LIMIT_REACHED = 'ROOM_LIMIT_REACHED',
  ROOM_NO_ACCESS = 'ROOM_NO_ACCESS',
  ROOM_NOT_OPEN = 'ROOM_NOT_OPEN',
  PLAYERS_LIMIT_REACHED = 'PLAYERS_LIMIT_REACHED'
}

export class HttpErrorMessages {
  static Values = Values;

  public static contains(message: string): boolean {
    return Object.values(Values)
      .map(value => value as string)
      .includes(message);
  }
}
