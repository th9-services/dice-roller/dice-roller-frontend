export interface HttpErrorField {
  field: string;
  reason: string;
}
