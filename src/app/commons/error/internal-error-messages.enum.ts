enum Values {
  SSE_TIMEOUT = 'No activity within',
  FAILED_TO_FETCH_SSE = 'Failed to fetch'
}

export class InternalErrorMessages {
  static Values = Values;

  public static contains(message: string): boolean {
    if (!message) {
      return false;
    }

    return Object.values(Values)
      .map(value => value as string)
      .some(value => message.includes(value));
  }
}
