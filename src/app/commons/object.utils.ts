import * as deepEqual from 'fast-deep-equal';

export class ObjectUtils {

  public static equals<T>(a: T, b: T): boolean {
    return deepEqual(a, b);
  }

  public static clone<T>(value: T): T {
    return JSON.parse(JSON.stringify(value));
  }
}
