import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

interface DicesResource {
  dices: number[];
}

@Injectable()
export class DicesFormService {
  private readonly DEFAULT_DICES_RESOURCE_URL = './assets/resources/default-dices.json';

  readonly MAX_DICE_VALUE: number = 1000;

  constructor(private httpClient: HttpClient) { }

  public getDefaultDices(): Observable<number[]> {
    return this.httpClient.get<DicesResource>(this.DEFAULT_DICES_RESOURCE_URL)
      .pipe(map(body => body.dices));
  }
}
