import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { IsPositivePipe } from '../../forms/is-positive.pipe';

@Component({
  selector: 'dr-add-dice',
  templateUrl: './add-dice.component.html',
  styleUrls: ['./add-dice.component.scss']
})
export class AddDiceComponent implements OnInit {
  private localDefaultDices: number[];

  @Input()
  diceForm: FormControl;
  @Input()
  occupiedDices: number[];
  @Input()
  defaultDices: number[];
  @Input()
  maxDiceValue: number;
  @Output()
  delete: EventEmitter<void> = new EventEmitter();

  filteredDices: Observable<number[]>;
  autocompleteForm: FormControl;
  dicePrefix: string;

  constructor(private translateService: TranslateService,
              private isPositivePipe: IsPositivePipe) {
  }

  ngOnInit(): void {
    this.initDefaults();
  }

  private initDefaults(): void {
    this.initAutocompleteForm();
    this.localDefaultDices = this.defaultDices.slice();
    this.initDicesFilter();
    this.dicePrefix = this.translateService.instant('general.dice.prefix');
  }

  private initAutocompleteForm(): void {
    this.autocompleteForm = new FormControl(this.diceForm.value, this.diceForm.validator);
  }

  private initDicesFilter(): void {
    this.filteredDices = this.autocompleteForm.valueChanges
      .pipe(
        startWith(null),
        map(dice => this.filterDices(dice))
      );
  }

  private filterDices(value: number): number[] {
    let filtered = this.createSuggestion(value)
      .concat(this.localDefaultDices)
      .filter(dice => !this.occupiedDices.some(occupied => occupied === dice))
      .filter(dice => dice <= this.maxDiceValue);

    filtered = [... new Set(filtered)];

    if (value) {
      filtered = filtered
        .filter(dice => dice.toString().includes(value.toString()));
    }

    return filtered.sort((a, b) => a - b);
  }

  private createSuggestion(value: number): number[] {
    if (!this.isPositivePipe.transform(value)) {
      return [];
    }

    const stringValue = value.toString();
    const lastDigit = stringValue.charAt(stringValue.length - 1);
    return [
      value,
      parseInt(stringValue + lastDigit, 10),
      value * 10,
      value * 100,
    ];
  }

  public saveSelectedOption(event: MatAutocompleteSelectedEvent): void {
    this.diceForm.setValue(event.option.value);
  }

  public onInputBlur(): void {
    if (this.isDiceValid(this.autocompleteForm.value)) {
      this.diceForm.setValue(this.autocompleteForm.value);
    }
  }

  private isDiceValid(value: number): boolean {
    return this.isPositivePipe.transform(value)
      && value <= this.maxDiceValue
      && !this.occupiedDices.includes(this.autocompleteForm.value);
  }
}
