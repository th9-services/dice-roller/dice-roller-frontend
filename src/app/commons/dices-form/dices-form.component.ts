import { filter, takeUntil } from 'rxjs/operators';
import { BaseDestroyDirective } from 'src/app/base-destroy.directive';

import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormControl } from '@angular/forms';

import { DicesFormService } from './dices-form.service';

@Component({
  selector: 'dr-dices-form',
  templateUrl: './dices-form.component.html',
  styleUrls: ['./dices-form.component.scss']
})
export class DicesFormComponent extends BaseDestroyDirective implements OnInit {
  occupiedDices: number[] = [];
  defaultDices: number[];
  maxDiceValue: number = this.diceFormService.MAX_DICE_VALUE;

  @Input()
  dicesForm: FormArray;

  constructor(private diceFormService: DicesFormService) {
    super();
  }

  ngOnInit() {
    if (this.dicesForm) {
      this.occupiedDices = this.dicesForm.getRawValue();
      this.addDiceControlOnArrayChange();
    }

    this.diceFormService.getDefaultDices()
      .subscribe(dices => this.defaultDices = dices);
  }

  private addDiceControlOnArrayChange(): void {
    this.dicesForm.valueChanges
      .pipe(
        filter((dices: number[]) => !dices.some(dice => !dice)),
        takeUntil(this.unsubscribe)
      )
      .subscribe(dices => {
        this.occupiedDices = dices;
        this.dicesForm.push(new FormControl(null));
      });
  }

  public deleteDice(formIndex: number): void {
    this.dicesForm.removeAt(formIndex);
    this.occupiedDices = this.dicesForm.getRawValue();
  }
}
