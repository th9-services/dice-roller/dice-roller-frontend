import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { TranslateModule } from '@ngx-translate/core';
import { FormPipesModule } from '../forms/form-pipes.module';
import { AddDiceComponent } from './add-dice/add-dice.component';
import { DicesFormComponent } from './dices-form.component';
import { DicesFormService } from './dices-form.service';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    FormPipesModule,
    MatAutocompleteModule,
    MatIconModule
  ],
  declarations: [
    DicesFormComponent,
    AddDiceComponent
  ],
  providers: [
    DicesFormService
  ],
  exports: [
    DicesFormComponent
  ]
})
export class DicesFormModule { }
