import { ScreenWidthSize } from './screen-width-size.enum';

export interface ScreenSize {
  innerWidth: number;
  innerHeight: number;
  widthSize: ScreenWidthSize;
}
