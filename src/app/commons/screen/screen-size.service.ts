import { Injectable } from '@angular/core';
import { fromEvent, Observable } from 'rxjs';
import { debounceTime, map, share } from 'rxjs/operators';
import { ScreenSize } from './screen-size.model';
import { ScreenWidthSize } from './screen-width-size.enum';


@Injectable()
export class ScreenSizeService {
  public static SMALL_WIDTH_TRESHOLD = 800;
  public static MEDIUM_WIDTH_TRESHOLD = 1920;

  constructor() { }

  public getCurrentScreenSize(): ScreenSize {
    return this.createScreenSize(window);
  }

  public getScreenResizeStream(): Observable<ScreenSize> {
    return fromEvent(window, 'resize')
      .pipe(
        debounceTime(100),
        map(event => event.target as Window),
        map(target => this.createScreenSize(target)),
        share()
      );
  }

  private createScreenSize(target: Window): ScreenSize {
    return {
      innerWidth: target.innerWidth,
      innerHeight: target.innerHeight,
      widthSize: this.getSScreenWidthSize(target.innerWidth)
    };
  }

  private getSScreenWidthSize(width: number): ScreenWidthSize {
    if (width <= ScreenSizeService.SMALL_WIDTH_TRESHOLD) {
      return ScreenWidthSize.SMALL;
    } else if (width <= ScreenSizeService.MEDIUM_WIDTH_TRESHOLD) {
      return ScreenWidthSize.MEDIUM;
    }

    return ScreenWidthSize.LARGE;
  }
}
