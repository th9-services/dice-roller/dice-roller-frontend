import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScreenSizeService } from './screen-size.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    ScreenSizeService
  ]
})
export class ScreenModule { }
