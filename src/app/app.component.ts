import { Component, OnInit } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { TranslateService } from '@ngx-translate/core';
import { I18nService } from './i18n/i18n.service';
import { LanguageSupport } from './i18n/language-support.model';
import { NavigationService, NavigationLink } from './navigation.service';

@Component({
  selector: 'dr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  languageOptions: LanguageSupport[];
  selectedLang: string;

  constructor(private translateService: TranslateService,
              private navigationService: NavigationService,
              private i18nService: I18nService) {
  }

  get link(): NavigationLink {
    return this.navigationService.getNavigation();
  }

  ngOnInit(): void {
    this.initLang();
  }

  private initLang(): void {
    this.i18nService.getSupportedLanguages()
      .subscribe(langs => {
        this.initLangOptions(langs);
        this.initUsedTranslation();
      });
  }

  private initLangOptions(languages: LanguageSupport[]): void {
    this.languageOptions = languages;
    this.selectedLang = this.i18nService.getCachedLang() || this.getDefaultLangFromConfig().code;
  }

  private getDefaultLangFromConfig(): LanguageSupport {
    return this.languageOptions.find(lang => lang.default) || this.languageOptions[0];
  }

  private initUsedTranslation(): void {
    this.translateService.setDefaultLang(this.selectedLang);
    this.translateService.use(this.selectedLang);
  }

  public onLangChange(change: MatSelectChange): void {
    this.translateService.use(change.value);
    this.i18nService.cacheLang(change.value);
  }
}
