import { Subject } from 'rxjs';

import { Component, Directive, OnDestroy } from '@angular/core';

@Directive()
export class BaseDestroyDirective implements OnDestroy {
  protected unsubscribe: Subject<void> = new Subject();

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
