export const environment = {
  production: true,
  hostUrl: window.location.protocol + '//' + window.location.hostname + ':8090'
};
